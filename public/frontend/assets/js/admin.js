    function previewfile(input) {
        var file = $("#post-image").get(0).files[0];
        if (file) {
            console.log("Image Added");
            var reader = new FileReader();
            reader.onload = function() {
                // $('#previewImg').attr("src", reader.result);
                $(".box").css("background-image", "url(" + reader.result + ")");
                $(".box").css("background-size", "auto");

                $(".trainer-icon").hide();
            }
            reader.readAsDataURL(file);
        }

    }

    function previewfileheader(input) {
        var file = $("#cover_image").get(0).files[0];
        if (file) {
            console.log("Image Added");
            var reader = new FileReader();
            reader.onload = function() {
                // $('#previewImg').attr("src", reader.result);
                $(".box").css("background-image", "url(" + reader.result + ")");
                $(".box").css("background-size", "auto");

                $(".trainer-icon").hide();
            }
            reader.readAsDataURL(file);
        }

    }

    function previewfile1(input) {
        var file = $("#featured-image").get(0).files[0];
        if (file) {
            var reader = new FileReader();
            reader.onload = function() {
                $(".box-1").css("background-image", "url(" + reader.result + ")");
                $(".featured-icon").hide();
            }
            reader.readAsDataURL(file);
        }

    }