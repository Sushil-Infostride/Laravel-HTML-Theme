<?php

use App\Http\Controllers\admin\AdminAboutController;
use App\Http\Controllers\admin\AdminContactController;
use App\Http\Controllers\admin\AdminHeaderController;
use App\Http\Controllers\admin\AdminTrainerController;
use App\Http\Controllers\frontend\AboutController;
use App\Http\Controllers\frontend\ContactController;
use App\Http\Controllers\frontend\CourseController;
use App\Http\Controllers\frontend\EventsController;
use App\Http\Controllers\frontend\HomeController;
use App\Http\Controllers\frontend\PricingController;
use App\Http\Controllers\frontend\TrainerController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\tempcontroller;
use App\Http\Controllers\CourseDetailsController;
use App\Http\Controllers\admin\AdminCourseInfoController;
use App\Http\Controllers\admin\AdminEventController;
use App\Http\Controllers\admin\FeatureController;
use App\Http\Controllers\admin\TestimonialController;
use App\Http\Controllers\userController;
use Illuminate\Support\Facades\Redirect;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Frontend Controllers
Route::get('/', [HomeController::class, "index"]);
Route::get('/about', [AboutController::class, "index"]);
Route::get('/coursedetails', [CourseDetailsController::class, "index"]);
Route::get('/courses', [CourseController::class, "index"]);
Route::get('/events', [EventsController::class, "index"]);
Route::get('/contact', [ContactController::class, "index"]);
Route::get('/pricing', [PricingController::class, "index"]);
Route::get('/trainers', [TrainerController::class, "index"]);
Route::get('/temp', [tempcontroller::class, "index"]);
Route::post('/send', [ContactController::class, "sendEmail"])->name('contact.send');
Route::get('/admin/courses', [CourseInfoController::class, "display"]);
Route::get('/admin/courses1', [CourseInfoController::class, "index"]);

//Authentications
Route::get('/login', function () {
    if (session()->has('user')) {
        return redirect('/admin/header');
    } else {
        return view('auth/login');
    }
});


Route::post('/login', [userController::class, "index"]);
Route::group(['middleware' => 'login'], function () {

Route::get('/logout', function () {
    if (session()->has('user')) {
        session()->pull('user');
        return redirect('/login');
    }
});
Route::get('/admin/courses/create', [AdminCourseInfoController::class, "create"]);
Route::post('/admin/courses/create', [AdminCourseInfoController::class, "store"])->name('courses.store');
Route::get('/admin/courses/edit/{id}', [AdminCourseInfoController::class, "editCourse"]);
Route::put('/admin/courses/edit', [AdminCourseInfoController::class, "update"])->name('courses.update');
Route::get('/admin/courses/delete/{id}', [AdminCourseInfoController::class, "delete"]);

//admin navbar conroller
Route::get('/admin/home', [AdminCourseInfoController::class, "home"]);
Route::get('/admin/about', [AdminCourseInfoController::class, "about"]);
Route::get('/admin//coursedetails', [AdminCourseInfoController::class, "coursedetails"]);
Route::get('/admin/courses', [AdminCourseInfoController::class, "courses"]);
Route::get('/admin/contact', [AdminCourseInfoController::class, "contact"]);
Route::get('/admin/pricings', [AdminCourseInfoController::class, "pricing"]);

//trainer controller
Route::get('/admin/trainers/create', [AdminTrainerController::class, "create"]);
Route::post('/admin/trainers/create', [AdminTrainerController::class, "store"])->name('trainers.store');
Route::get('/admin/trainers', [AdminTrainerController::class, "trainers"]);
Route::get('/admin/trainers/edit/{id}', [AdminTrainerController::class, "editCourse"]);
Route::post('/admin/trainers/edit', [AdminTrainerController::class, "update"])->name('trainers.update');
Route::get('/admin/trainers/delete/{id}', [AdminTrainerController::class, "delete"]);

//admin header controller
Route::get('admin/header', [AdminHeaderController::class, "index"]);
Route::get('/admin/header/create', [AdminHeaderController::class, "header"]);
Route::post('/admin/header/create', [AdminHeaderController::class, "store"])->name('header.store');
Route::get('/admin/header/edit/{id}', [AdminHeaderController::class, "edit"]);
Route::post('/admin/header/edit', [AdminHeaderController::class, "update"])->name('header.update');
Route::get('/admin/header/delete/{id}', [AdminHeaderController::class, "delete"]);

//admin testimonial controller
Route::get('admin/testimonial', [TestimonialController::class, "index"]);
Route::get('admin/testimonial/create', [TestimonialController::class, "create"]);
Route::post('admin/testimonial/store', [TestimonialController::class, "store"]);
Route::get('admin/testimonial/edit/{id}', [TestimonialController::class, "edit"]);
Route::post('admin/testimonial/update', [TestimonialController::class, "update"])->name('testimonial.create');
Route::get('admin/testimonial/delete/{id}', [TestimonialController::class, "delete"]);

//admin features controller
Route::get('admin/feature', [FeatureController::class, "index"]);
Route::get('admin/feature/create', [FeatureController::class, "create"]);
Route::post('admin/feature/create', [FeatureController::class, "store"])->name('submitdata');
Route::get('admin/feature/edit/{id}', [FeatureController::class, "edit"]);
Route::post('admin/feature/update', [FeatureController::class, "update"])->name('feature.create');
Route::get('admin/feature/delete/{id}', [FeatureController::class, "delete"]);

//admin features controller
Route::get('admin/about', [AdminAboutController::class, "index"]);
Route::get('admin/about/create', [AdminAboutController::class, "create"]);
Route::post('admin/about/create', [AdminAboutController::class, "store"]);
Route::get('admin/about/edit/{id}', [AdminAboutController::class, "edit"]);
Route::post('admin/about/edit', [AdminAboutController::class, "update"]);

//admin events controller
Route::get('admin/events', [AdminEventController::class, "index"]);
Route::get('admin/events/create', [AdminEventController::class, "create"]);
Route::post('admin/events/create', [AdminEventController::class, "store"]);
Route::get('admin/events/edit/{id}', [AdminEventController::class, "edit"]);
Route::post('admin/events/edit', [AdminEventController::class, "update"]);

//admin contact controller
Route::get('admin/contact', [AdminContactController::class, "index"]);
Route::get('admin/contact/create', [AdminContactController::class, "create"]);
Route::post('admin/contact/create', [AdminContactController::class, "store"]);
Route::get('admin/contact/edit/{id}', [AdminContactController::class, "edit"]);
Route::post('admin/contact/edit', [AdminContactController::class, "update"]);
});
