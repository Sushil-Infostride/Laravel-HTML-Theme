<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use App\Model\courses;
use App\Model\trainers;
use Illuminate\Http\Request;

class AdminCourseInfoController extends Controller
{
    public function index()
    {
        $courses = courses::all();
        return view('admin.courses', compact('courses'));
    }
    public function create()
    {
        return view('admin.courses.create');
    }

    // Store data to database
    public function store(Request $request)
    {
        $title = $request->title;
        $price = $request->price;
        $content = $request->content;
        $trainer = $request->trainer_name;
        $field_area = $request->field_area;
        $featured_image = $request->file('featured-image');
        $featured_image_name = time() . '-featured-.' . $featured_image->extension();
        $featured_image->move(public_path('upload-featured'), $featured_image_name);
        $trainer_image = $request->file('trainer-image');
        $trainer_image_name = time() . '-trainer-.' . $trainer_image->extension();
        $trainer_image->move(public_path('upload-trainer'), $trainer_image_name);

        $courses = new courses();
        $courses->title = $title;
        $courses->content = $content;
        $courses->field_area = $field_area;
        $courses->price = $price;
        $courses->trainer_name = $trainer;

        $courses->featured_image = $featured_image_name;
        $courses->trainer_image = $trainer_image_name;
        $courses->save();
        return back()->with('course_added', 'Course Post that you have created has been successfuly saved and can be viewed at ');
    }

    public function coursedetails()
    {
        return view('admin.courses-details');
    }

    // Show data from database
    public function courses()
    {
        $courses = courses::all();
        return view('admin.courses.course', compact('courses'));
    }

    public function contact()
    {
        return view('admin.contact');
    }

    //Render the Edit page with particular id Content
    public function editCourse($id)
    {
        $courses = courses::find($id);
        return view('admin.courses.edit', compact('courses'));
    }

    // Update data
    public function update(Request $request)
    {
        $courses = courses::find($request->id);
        $title = $request->title;
        $price = $request->price;
        $content = $request->content;
        $trainer = $request->trainer_name;
        $trainer_old_image = $request->trainer_old_image;
        $featured_old_image = $request->featured_old_image;
        $field_area = $request->field_area;
        $featured_image = $request->file('featured-image');
        $trainer_image = $request->file('trainer-image');

        // featured Image Update
        if ($featured_image == '') {
            $featured_image_name = $featured_old_image;
            $courses->featured_image = $featured_image_name;
        } else {
            $featured_image_name = time() . '-updated-featured-.' . $featured_image->extension();
            $featured_image->move(public_path('upload-featured'), $featured_image_name);
            $courses->featured_image = $featured_image_name;
        }

        // Trainer Image Update'
        
        if ($trainer_image == '') {
            $trainer_image_name = $trainer_old_image;
            $courses->trainer_image = $trainer_image_name;
        } else {
            $trainer_image_name = time() . 'updated-trainer-.' . $trainer_image->extension();
            $trainer_image->move(public_path('upload-trainer'), $trainer_image_name);
            $courses->trainer_image = $trainer_image_name;
        }


        $courses->title = $title;
        $courses->content = $content;
        $courses->field_area = $field_area;
        $courses->price = $price;
        $courses->trainer_name = $trainer;
        $courses->save();
        return back()->with('course_updated', 'Course Post that you have created has been successfuly Updated and can be viewed at ');
    }
    //Delete Functionality
    public function delete($id)
    {
        $courses = courses::find($id);
        unlink(public_path('upload-featured') . '/' . $courses->featured_image);
        unlink(public_path('upload-trainer') . '/' . $courses->trainer_image);
        $courses->delete();
        return back()->with('course_deleted', 'Course Post that you have created has been successfuly Deleted ');
    }
}
