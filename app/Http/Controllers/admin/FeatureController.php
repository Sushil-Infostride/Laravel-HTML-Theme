<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use App\Model\features;
use Illuminate\Http\Request;

class FeatureController extends Controller
{
    public function index()
    {
        $features = features::all();
        return view('admin.features.index', compact('features'));
    }
    public function create()
    {
        return view('admin.features.create');
    }
    public function store(Request $request)
    {
        foreach ($request->title as $key => $title) {
            $feature = new features();
            $feature->title = $title;
            $feature->icon = $request->icon[$key];
            $feature->save();

        }
        return redirect('admin/feature');
    }
    public function edit($id)
    {
        $features = features::find($id);
        return view('admin.features.edit', compact('features'));
    }
    public function update(Request $request)
    {
            $feature = features::find($request->id);
            $feature->title = $request->title;
            $feature->icon = $request->icon;
            $feature->save();
        return redirect('admin/feature');
    }
    public function delete($id)
    {
        $features = features::find($id);
        if ($features->avatar == '') {
        } else {
            unlink(public_path('feature-image') . '/' . $features->avatar);
        }
        $features->delete();
        return back()->with('features_deleted', 'Course Post that you have created has been successfuly Deleted ');
    }
}
