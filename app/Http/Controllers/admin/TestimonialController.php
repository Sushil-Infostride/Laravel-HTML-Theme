<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use App\Model\testimonials;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    public function index()
    {
        $testimonial=testimonials::all();
        return view('admin.testimonial.index',compact('testimonial'));
    }
    public function create()
    {
        return view('admin.testimonial.create');
    }
    public function store(Request $request)
    {
        $name = $request->name;
        $post = $request->post;
        $content = $request->content;
        $avatar = $request->file('avatar');
        $avatar_name = time() . '-testimonial-.' . $avatar->extension();
        $avatar->move(public_path('Testimonial-image'), $avatar_name);

        $testimonials = new testimonials();
        $testimonials->name = $name;
        $testimonials->post = $post;
        $testimonials->content = $content;
        $testimonials->avatar = $avatar_name;
        $testimonials->save();
        return back()->with('testimonial_added', 'New testimonial has been added');
    }
    public function edit($id){
        $testimonials = testimonials::find($id);
        return view('admin.testimonial.edit', compact('testimonials'));
    }
    public function update(Request $request){

        $name = $request->name;
        $post = $request->post;
        $content = $request->content;
        $avatar = $request->file('avatar');
        $avatar_old_image= $request->avatar_old_image;

        $testimonials = testimonials::find($request->id);
        $testimonials->name = $name;
        $testimonials->post = $post;
        $testimonials->content = $content;

        if($avatar==''){
            $avatar_name=$avatar_old_image;
            $testimonials->avatar = $avatar_name;
        }else{
            $avatar_name = time() . '-testimonial-updated.' . $avatar->extension();
            $avatar->move(public_path('Testimonial-image'), $avatar_name);
            $testimonials->avatar = $avatar_name;
        }
        $testimonials->save();
        return back()->with('testimonials_updated', 'testimonials updated successfully ');
    }
    public function delete($id){
        $testimonials=testimonials::find($id);
        if($testimonials->avatar==''){

        }else{
        unlink(public_path('Testimonial-image').'/'.$testimonials->avatar);
        }
        $testimonials->delete();
        return back()->with('testimonials_deleted' ,'Course Post that you have created has been successfuly Deleted ');
      }
}
