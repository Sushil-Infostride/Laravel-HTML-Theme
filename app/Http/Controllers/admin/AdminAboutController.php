<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use App\Model\about;
use Illuminate\Http\Request;

class AdminAboutController extends Controller
{
    public function index(){
        $about=about::all();
        return view('admin.about.index',compact('about'));
    }
    public function create(){
        return view('admin.about.create');
    }
    public function store(Request $request){
        $about=new about();
        $about->btitle= $request->btitle;
        $about->bcontent= $request->bcontent;
        $about->main_title= $request->main_title;
        $about->base_content= $request->base_content;
        $about->bullet1= $request->bullet1;
        $about->bullet2= $request->bullet2;
        $about->bullet3= $request->bullet3;
        $cover_image = $request->file('cover_image');
        $cover_image_name = time() . '-about-image-.' . $cover_image->extension();
        $cover_image->move(public_path('about-image'), $cover_image_name);
        $about->cover_image=$cover_image_name;
        $about->save();
        return back()->with('about_page_created','About page Created Succesfully');
    }
    public function edit($id){
        $about=about::find($id);
        return view('admin.about.edit',compact('about'));
    }

    public function update(Request $request)
    {
        $about = about::find($request->id);
        $about->btitle= $request->btitle;
        $about->bcontent= $request->bcontent;
        $about->main_title= $request->main_title;
        $about->base_content= $request->base_content;
        $about->bullet1= $request->bullet1;
        $about->bullet2= $request->bullet2;
        $about->bullet3= $request->bullet3;

        $cover_image=$request->file('cover_image');
        $cover_old_image=$request->cover_old_image;

        if ($cover_image == '') {
            $cover_image_name = $cover_old_image;
            $about->cover_image = $cover_image_name;
        } else {
            $cover_image_name = time() . 'about-image.' . $cover_image->extension();
            $cover_image->move(public_path('about-image'), $cover_image_name);
            $about->cover_image = $cover_image_name;
        }
        $about->save();
        return back()->with('about_page_updated', 'Page updated successfully ');
    }
}
