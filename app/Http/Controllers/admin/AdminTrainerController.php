<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use App\Model\trainer;
use Illuminate\Http\Request;

class AdminTrainerController extends Controller
{
    public function create()
    {
        return view('admin.trainer.create');
    }
    public function store(Request $request)
    {
        $trainer = $request->title;
        $instagram = $request->instagram;
        $facebook = $request->facebook;
        $twitter = $request->twitter;
        $linkedin = $request->linkedin;
        $content = $request->content;
        $field_area = $request->field_area;
        $trainer_image = $request->file('trainer-image');
        $trainer_image_name = time() . '-trainer-.' . $trainer_image->extension();
        $trainer_image->move(public_path('trainers-upload-trainer'), $trainer_image_name);

        $trainers = new trainer();
        $trainers->trainer_name = $trainer;
        $trainers->content = $content;
        $trainers->field_area = $field_area;
        $trainers->instagram = $instagram;
        $trainers->facebook = $facebook;
        $trainers->twitter = $twitter;
        $trainers->linkedin = $linkedin;
        $trainers->trainer_image = $trainer_image_name;
        $trainers->save();
        return back()->with('trainer_added', 'trainer that you have added has been successfuly saved and can be viewed at ');
    }
    public function trainers()
    {
        $trainers = trainer::all();
        return view('admin.trainer.trainers', compact('trainers'));
    }

    public function editCourse($id){
        $courses=trainer::find($id);
        return view('admin.trainer.edit',compact('courses'));
      }
    public function update(Request $request)
    {
        $trainers = trainer::find($request->id);
        $trainer = $request->title;
        $instagram = $request->instagram;
        $facebook = $request->facebook;
        $twitter = $request->twitter;
        $linkedin = $request->linkedin;
        $content = $request->content;
        $trainer_old_image=$request->trainer_old_image;
        $field_area = $request->field_area;
        $trainer_image = $request->file('trainer-image');
        if($trainer_image ==''){
            $trainer_image_name=$trainer_old_image;
            $trainers->trainer_image=$trainer_image_name;
        }
        else{
            $trainer_image_name = time() . 'updated-trainer-.' . $trainer_image->extension();
            $trainer_image->move(public_path('trainers-upload-trainer'), $trainer_image_name);
            $trainers->trainer_image = $trainer_image_name;
        }

        $trainers->trainer_name = $trainer;
        $trainers->content = $content;
        $trainers->field_area = $field_area;
        $trainers->facebook = $facebook;
        $trainers->linkedin = $linkedin;
        $trainers->instagram = $instagram;
        $trainers->twitter = $twitter;

        $trainers->save();
        return back()->with('trainer_updated', 'Course Post that you have created has been successfuly Updated and can be viewed at ');
    }
    public function delete($id)
    {
        $trainers = trainer::find($id);
        unlink(public_path('trainers-upload-trainer') . '/' . $trainers->trainer_image);
        $trainers->delete();
        return back()->with('deleted_trainer', 'Course Post that you have created has been successfuly Deleted ');
    }
}
