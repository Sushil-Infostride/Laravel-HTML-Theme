<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use App\Model\contact as AppContact;
use App\Mail\Contact;
use Illuminate\Http\Request;

class AdminContactController extends Controller
{
    public function index(){
        $contacts=AppContact::all();
        return view('admin.contact.index',compact('contacts'));

    }
    public function create(){
        return view('admin.contact.create');
    }
    public function store(Request $request){
        //btitle	bcontent	iframe	location_icon	location	email_icon	email_add	address_icon	address
        $contacts=new AppContact();
        $contacts->btitle= $request->btitle;
        $contacts->bcontent= $request->bcontent;
        $contacts->iframe= $request->iframe;
        $contacts->location_icon= $request->location_icon;
        $contacts->location= $request->location;
        $contacts->email_icon= $request->email_icon;
        $contacts->email_add= $request->email_add;
        $contacts->address_icon= $request->address_icon;
        $contacts->address= $request->address;
        $contacts->save();
        return back()->with('contacts_page_created','contacts page Created Succesfully');
    }
    public function edit($id){
        $contacts=AppContact::find($id);
        return view('admin.contact.edit',compact('contacts'));
    }
    public function update(Request $request){
        $contacts=AppContact::find($request->id);
        $contacts->btitle= $request->btitle;
        $contacts->bcontent= $request->bcontent;
        $contacts->iframe= $request->iframe;
        $contacts->location_icon= $request->location_icon;
        $contacts->location= $request->location;
        $contacts->email_icon= $request->email_icon;
        $contacts->email_add= $request->email_add;
        $contacts->address_icon= $request->address_icon;
        $contacts->address= $request->address;
        $contacts->save();
        return back()->with('contacts_page_created','contacts page Created Succesfully');
    }
}
