<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Model\events;
use Illuminate\Http\Request;

class AdminEventController extends Controller
{
    public function index(){
        $events=events::all();
        return view('admin.events.index',compact('events'));
    }

    public function create(){
        return view('admin.events.create');
    }

    public function store(Request $request){
        $events=new events();
        $events->title= $request->title;
        $events->main_content= $request->main_content;

        $cover_image = $request->file('cover_image');
        $cover_image_name = time() . '-events-image-.' . $cover_image->extension();
        $cover_image->move(public_path('events-image'), $cover_image_name);
        $events->cover_image=$cover_image_name;
        $events->save();
        return back()->with('events page created','Event page Created Succesfully');
    }

    public function edit($id){
        $events=events::find($id);
        return view('admin.events.edit',compact('events'));
    }

    public function update(Request $request)
    {
        $events = events::find($request->id);
        $events->title= $request->title;
        $events->main_content= $request->main_content;
        $cover_image = $request->file('cover_image');
        $cover_old_image=$request->cover_old_image;

        if ($cover_image == '') {
            $cover_image_name = $cover_old_image;
            $events->cover_image = $cover_image_name;
        } else {
            $cover_image_name = time() . 'events-image-updated.' . $cover_image->extension();
            $cover_image->move(public_path('events-image'), $cover_image_name);
            $events->cover_image = $cover_image_name;
        }
        $events->save();
        return back()->with('about_page_updated', 'Page updated successfully ');
    }
}
