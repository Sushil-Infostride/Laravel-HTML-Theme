<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Model\header;

use Illuminate\Http\Request;

class AdminHeaderController extends Controller
{
    public function index(){
        $headers = header::all();
        return view('admin.header.index', compact('headers'));
    }
    public function header()
    {
        return view('admin.header.create');
    }
    public function store(Request $request)
    {
        $title = $request->title;
        $subtitle = $request->subtitle;
        $content = $request->content;
        $button_name = $request->button_name;
        $button_link = $request->button_link;
        // Image upload
        $cover_image = $request->file('cover_image');
        $cover_image_name = time() . '-header-image-.' . $cover_image->extension();
        $cover_image->move(public_path('header-image'), $cover_image_name);

        $header = new header();
        $header->title = $title;
        $header->subtitle = $subtitle;
        $header->content = $content;
        $header->button_name = $button_name;
        $header->button_link = $button_link;
        $header->cover_image = $cover_image_name;
        $header->save();
        return back()->with('header_added', 'Header added succefully');
    }
    public function edit($id)
    {
        $header = header::find($id);
        return view('admin.header.edit', compact('header'));
    }
    public function update(Request $request)
    {
        $header = header::find($request->id);
        $title = $request->title;
        $subtitle = $request->subtitle;
        $content = $request->content;
        $button_name = $request->button_name;
        $button_link = $request->button_link;
        $cover_old_image=$request->cover_old_image;
        // Image upload
        $cover_image = $request->file('cover_image');

        // featured Image Update
        if ($cover_image == '') {
            $cover_image_name = $cover_old_image;
            $header->cover_image = $cover_image_name;
        } else {
            $cover_image_name = time() . 'cover-image-updated.' . $cover_image->extension();
            $cover_image->move(public_path('header-image'), $cover_image_name);
            $header->cover_image = $cover_image_name;
        }
        $header->title = $title;
        $header->subtitle = $subtitle;
        $header->content = $content;
        $header->button_name = $button_name;
        $header->button_link = $button_link;
        $header->save();
        return back()->with('header_updated', 'Header updated successfully ');
    }
    public function delete($id){
        $header=header::find($id);
        if($header->cover_image==''){

        }else{
        unlink(public_path('header-image').'/'.$header->cover_image);
        }
        $header->delete();
        return back()->with('header_deleted' ,'Course Post that you have created has been successfuly Deleted ');
      }
}
