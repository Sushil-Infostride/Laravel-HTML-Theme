<?php

namespace App\Http\Controllers;

use App\Model\User;
use Illuminate\Http\Request;


class userController extends Controller
{
    public function index(Request $request){

      if(User::where('email', '=', $request->email)->exists() && User::where('password', '=', $request->password)->exists()){
          $data=$request->input();
          $request->session()->put('user',$data['email']);
          return redirect('/admin/header');
      }
      else{
        return back()->with('p_wrong', 'Invalid Credentials ');
      }
    }
}
