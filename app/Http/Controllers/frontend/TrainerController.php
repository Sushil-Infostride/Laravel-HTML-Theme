<?php

namespace App\Http\Controllers\frontend;
use App\Model\trainer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TrainerController extends Controller
{
    public function index(){
        $trainers=trainer::all();
        return view('frontend/trainers',compact('trainers'));
    }

}
