<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Model\Contact;
use App\Model\contact as AppContact;
use App\Mail\ContactMail;
use App\Mail\MailNotify;
use Illuminate\Support\Facades\Log;

class ContactController extends Controller
{
    public function index()
    {
        $contacts=AppContact::all();
        return view('frontend/contact',compact('contacts'));
    }
    public function  sendEmail(Request $request){
        $details = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->name,
            'subject'=>$request->subject,
            'message' => $request->message
         ];
         Mail::to('sushil124maurya@gmail.com')->send(new ContactMail($details));
         return back()->with('message-sent','Your message has been sent');


    }

    }

