<?php

namespace App\Http\Controllers\frontend;

use App\Model\events;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function index(){
        $events=events::all();
        return view('frontend\events',compact('events'));
    }
}
