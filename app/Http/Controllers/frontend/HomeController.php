<?php

namespace App\Http\Controllers\frontend;

use App\Model\about;
use App\Model\contact;
use App\Model\courses;
use App\Model\events;
use App\Model\features;
use App\Model\header;
use App\Http\Controllers\Controller;
use App\Model\trainer;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $about=about::all();
        $contacts=contact::all();
        $events=events::all();
        $trainer=trainer::all();
        $header = header::all();
        $features = features::all();
        $course=courses::all();

        return view('frontend\index',compact('about','contacts','events','trainer','header','features','course'));
    }
}
