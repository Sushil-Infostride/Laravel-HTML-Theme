<?php

namespace App\Http\Controllers\frontend;

use App\Model\about;
use App\Model\testimonials;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index(){
        $about=about::all();
        $testimonials=testimonials::all();
        return view('frontend/about',compact('about', 'testimonials'));
    }
}
