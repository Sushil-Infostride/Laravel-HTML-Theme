<?php

namespace App\Http\Controllers\frontend;

use App\courses;
use App\Http\Controllers\Controller;
use App\Model\courses as ModelCourses;
use Illuminate\Http\Request;


class CourseController extends Controller
{
    // public function index(){
    //     return view('course');
    // }
    public function index(){
        $courses=ModelCourses::all();
        return view('frontend\course',compact('courses'));
    }
}
