<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    public $fillable = ['name', 'email', 'phone', 'subject', 'message','btitle','bcontent','iframe','email_icon','email_add','location_icon','location','address','address_icon'];
}
