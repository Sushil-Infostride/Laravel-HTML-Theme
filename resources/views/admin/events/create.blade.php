@extends('admin.layout.main')
@section('main-container')
@section('title')
    <h1 class="title text-xl font-bold text-blue-700">Create Events Page</h1>
@endsection
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <div class="w-full overflow-x-hidden border-t flex flex-col">
        <div>

            <div class="md:grid md:grid-cols-2 mt-0 md:gap-6">
                <div class="mt-0 md:mt-0 md:col-span-2">
                    <form action="/admin/events/create" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <label for="Breadcrumb" class="block text-md font-medium text-gray-900">
                                            Title
                                        </label>
                                        <div class="mt-1 flex rounded-md shadow-sm">
                                            <input type="text" name="title" id="company-website"
                                                class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 p-2 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300"
                                                placeholder="Add a Title to your events">
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <label for="about" class="block text-md font-medium text-gray-700">
                                        Content
                                    </label>
                                    <div class="mt-1">
                                        <textarea id="about" name="main_content" rows="3"
                                            class="shadow-sm focus:ring-indigo-500 p-3 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md"
                                            placeholder="you@example.com"></textarea>
                                    </div>
                                    <div>
                                        <label class="block text-md font-medium text-gray-900 mt-2">
                                            Featured photo
                                        </label>
                                        <div
                                        class="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 w-2/3 border-gray-300 border-dashed rounded-md box">
                                        <div class="space-y-1 text-center ">
                                            <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none"
                                                viewBox="0 0 48 48" aria-hidden="true">
                                                <path
                                                    d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                                    stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            <div class="flex text-sm text-gray-600">
                                                <label for="cover_image"
                                                    class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                    <span>Upload a file</span>
                                                    <input type="file" class="hidden" id="cover_image" name="cover_image"
                                                        onchange="previewfileheader(this)">
                                                </label>
                                                <p class="pl-1">or drag and drop</p>
                                            </div>
                                            <p class="text-xs text-gray-500">
                                                PNG, JPG, GIF up to 10MB
                                            </p>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                    <button type="submit"
                                        class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                        Save
                                    </button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="{{ url('frontend/assets/js/admin.js') }}"></script>
    @endsection
