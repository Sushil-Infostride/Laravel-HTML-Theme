@extends('admin.layout.main')
@section('main-container')

    @push('DataTable-Css')
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" />
    @endpush

    {{-- Table to show a data --}}

    <table id="example" class="display" style="width:80%">
        <thead>
            <th>Title</th>
            <th>Content</th>
            <th>Featured Image</th>
            <th>Actions</th>

        </thead>
        <tbody>
            @foreach ($events as $event)
                <form action="/admin/events/edit" method="" enctype="multipart/form-data">
                    @csrf
                    <td>
                        <input type="text" name="title" class="myInput" readonly="readonly" value="{{ $event->title }}">
                    </td>
                    <td>
                        <textarea name="content" class="myInput" cols="30" rows="5" readonly="readonly">{{ $event->main_content }}</textarea>
                    </td>
                    {{-- Cover Image and Name --}}
                    <td>
                        <img style="border-radius:10%;border:1px solid black;padding:5px" width="100%"
                            src="{{ asset('events-image') }}/{{ $event->cover_image }}" class="img-fluid"
                            alt="...">
                    </td>

                    <td>
                        {{-- normal operation --}}
                        <div class="normal-operation">
                            {{-- Edit button --}}
                            <a href="/admin/events/edit/{{ $event->id }}">
                                <button type="button" id="any_butto"
                                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                    style="margin-bottom: 2px">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </a>

                            {{-- Delete Button --}}
                            <a href="/admin/events/delete/{{ $event->id }}"><button type="button" id="any_button"
                                    class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </a>
                        </div>
                        {{-- edit operation --}}
                        <div class="edit-operation">
                            {{-- Save Button --}}
                            <button type="submit" id="any_button"
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 my-2 px-4 rounded">
                                <i class="fas fa-check"></i>
                            </button>
                            {{-- Cancel Button --}}
                            <a href="/admin/events/"><button type="any" id="any_button"
                                    class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
                                    <i class="fas fa-window-close"></i>
                                </button>
                        </div>
                        </a>
                    </td>
                </form>
        </tbody>
        @endforeach
    </table>
    </div>
    {{-- Jquery Push --}}
    @push('jquery')
        <script src="https://code.jquery.com/jquery-2.0.0.js" integrity="sha256-iW43nTNM8LFseNmWKhV5FHFW1KcjVQMvzg3l9nPU4oc="
                crossorigin="anonymous"></script>
    @endpush

    {{-- DATA table Push --}}
    @push('data-table-script')
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    @endpush



    <script>
        $(document).ready(function() {
            $('.edit-operation').hide();
            $('.myInput').on("dblclick", function() {
                $(this).removeAttr("readonly");
                $('.edit-operation').show();
                $('.normal-operation').hide();

            });
            var table = $('#example').DataTable({
                dom: 'l<"toolbar">frtip',
                initComplete: function() {
                    $("div.toolbar")
                        .html(
                            '<a href="/admin/events/create"><button type="button"  id="add-more" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Add more </button></a>'
                        );
                },
                searching: false,
                paging: false,
                info: false
            });

        });
    </script>
@endsection
