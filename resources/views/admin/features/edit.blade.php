@extends('admin.layout.main')
@section('main-container')
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <!-- ======= Hero Section ======= -->
    <div class="w-full overflow-x-hidden border-t flex flex-col">
        <form action="/admin/feature/update" method="POST">
            @csrf
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Sr no.</th>
                        <th scope="col">Icon </th>
                        <th scope="col">Title</th>

                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <th scope="row">{{$features->id}}</th>
                        <input type="text" name="id" value="{{$features->id}}" id="icon" class="hidden">
                        <td><input type="text" name="icon" value="{{$features->icon}}" id="icon" class="form-control"></td>
                        <td><input type="text" name="title" value="{{$features->title}}" id="title" class="form-control"></td>
                       
                    </td>
                    </tr>
                </tbody>
            </table>
            <div class="text-center"><input type="submit" value="Save" class="btn btn-primary "></div>
        </form>
    </div>
    <!-- ======= Footer ======= -->
@endsection
