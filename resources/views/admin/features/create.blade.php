@extends('admin.layout.main')
@section('main-container')
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <!-- ======= Hero Section ======= -->
    <div class="w-full overflow-x-hidden border-t flex flex-col">
        <form action="/admin/feature/create" method="POST">
            @csrf
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Icon </th>
                        <th scope="col">Title</th>
                        <th scope="col"><a href="javascritp:;" class="btn btn-info addRow">+</a></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="text" name="icon[]" id="icon" class="form-control"></td>
                        <td><input type="text" name="title[]" id="title" class="form-control"></td>
                        <td scope="col"><a href="javascritp:;" class="btn btn-danger subRow">-</a></td>
                    </tr>
                </tbody>
            </table>
            <div class="text-center"><input type="submit" value="Save" class="btn btn-primary "></div>
        </form>
    </div>

    <script src="{{ url('frontend/assets/js/admin.js') }}"></script>
    <script>
        $('thead').on('click', '.addRow', function() {
            var tr =
                '<tr><td><input type="text" name="icon[]" id="icon" class="form-control"></td> <td><input type="text" name="title[]" id="title" class="form-control"></td><td scope="col"><a href="javascritp:;" class="btn btn-danger subRow">-</a></td></tr>'
            $('tbody').append(tr);
        })
        $('tbody').on('click', '.subRow', function() {
            $(this).parent().parent().remove();
        });


    </script>
    <!-- ======= Footer ======= -->
@endsection
