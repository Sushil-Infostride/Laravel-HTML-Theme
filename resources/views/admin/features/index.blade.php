@extends('admin.layout.main')
@section('main-container')
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <!-- ======= Hero Section ======= -->
    <div class="w-full overflow-x-hidden border-t flex flex-col">
        <a href="/admin/feature/create">
            <button type="button" id="any_butto"
                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                style="margin-bottom: 2px">
                Add Features
            </button>
        </a>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Sr no.</th>
                        <th scope="col">Icon </th>
                        <th scope="col">Title</th>
                        <th scope="col">Operation</th>
                    </tr>
                </thead>
                @foreach ($features as $feature )

                <tbody>
                    <tr>
                        <th scope="row">{{$feature->id}}</th>
                        <td><input type="text" name="icon[]" value="{{$feature->icon}}" readonly id="icon" class="form-control"></td>
                        <td><input type="text" name="title[]" value="{{$feature->title}}" readonly id="title" class="form-control"></td>
                        <td><a href="/admin/feature/edit/{{ $feature->id }}">
                            <button type="button" id="any_butto"
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                style="margin-bottom: 2px">
                                <i class="fas fa-edit"></i>
                            </button>
                        </a>
                        <a href="/admin/feature/delete/{{ $feature->id }}"><button type="button" id="any_button"
                            class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
                            <i class="far fa-trash-alt"></i>
                        </button>
                    </a>
                    </tr>
                </tbody>

                @endforeach
            </table>
    </div>

    <!-- ======= Footer ======= -->
@endsection
