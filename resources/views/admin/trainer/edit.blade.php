<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Post</title>
    <link rel="stylesheet" href="{{ url('frontend/assets/css/adminStyle.css') }}" />
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
</head>


<body>
    <div
        class="relative min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8 bg-gray-500 bg-no-repeat bg-cover relative items-center"
        style="background-image: url({{asset('bg.jpg')}});">

        <div class="absolute bg-black opacity-60 inset-0 z-0"></div>
        <div class="sm:max-w-lg w-full p-10 bg-white rounded-xl z-10">
            <div class="text-center">
                <h2 class="mt-5 text-3xl font-bold text-gray-900">
                    Edit Trainer
                </h2>
                <p class="mt-2 text-sm text-gray-400"> Lets Edit a trainer  or<a href="/admin/trainers" class="bg-none text-blue-300 hover:text-blue-700 "> See All trainers</a> or <a href="/admin/courses/create" class="bg-none text-blue-300 hover:text-blue-700 ">create a new one</a> </p>
            </div>
            <form class="mt-8 space-y-3" action="{{route('trainers.update')}}" method="POST"enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$courses->id}}"/>
                @if (Session::has('trainer_updated'))
                    <div class="flex items-center bg-blue-500 text-white text-sm font-bold px-4 py-3" role="alert">
                        <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path
                                d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z" />
                        </svg>
                        <p>{{ Session::get('trainer_updated') }} <a href="/admin/courses" class="bg-none text-blue-300 hover:text-blue-700 "> See All Posts</a> </p>
                    </div>
                @endif
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Trainer name</label>
                    <input name="title"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500"
                        type=""value="{{$courses->trainer_name}}" placeholder="Add a beautiful trainers to your post">
                </div>
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Area Of Field</label>
                    <input name="field_area"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500"
                        type=""value="{{$courses->field_area}}" placeholder="Eg. Web-design, Android, Marketing ....">
                </div>
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Facebook</label>
                    <input name="facebook"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500"
                        type="" value="{{$courses->facebook}}" placeholder="Add the price">
                </div>
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Linkedin Url</label>
                    <input name="linkedin"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500"value="{{$courses->linkedin}}"
                        type="" placeholder="Add the Linkedin url">
                </div>
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Instagram Url</label>
                    <input name="instagram"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500" value="{{$courses->instagram}}"
                        type="" placeholder="Add the Instagram url">
                </div>
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Twitter Url</label>
                    <input name="twitter"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500"value="{{$courses->twitter}}"
                        type="" placeholder="Add the Twitter url">
                </div>
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Content</label>
                    <textarea name="content"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500"
                        type=""  placeholder="Add Your Content">{{$courses->content}}</textarea>
                </div>
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Trainer name</label>
                    <input name="trainer_name"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500"
                        type=""value="{{$courses->trainer_name}}" placeholder="Add Trainer name">
                </div>

                 {{-- Trainer Image starts --}}
                 <div class="grid grid-cols-1 space-y-2 ">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Attach Trainer Image</label>
                    <div class="flex items-center justify-center w-full box" style="background-image: url('{{ asset('trainers-upload-trainer') }}/{{ $courses->trainer_image }}'); z-index:1">
                        <label
                            class="flex flex-col rounded-lg border-4 border-dashed w-full h-60 p-10 group text-center">
                            <input type="hidden" name="trainer_old_image" value="{{$courses->trainer_image }}">
                            <input type="file" id="post-image" class="hidden" name="trainer-image"
                                onchange="previewfile(this)">
                        </label>
                    </div>
                </div>{{-- Trainer Image ends --}}

                {{-- Submit section start --}}
                <div>
                    <button type="submit"
                        class="my-5 w-full flex justify-center bg-blue-500 text-gray-100 p-4  rounded-full tracking-wide
                                    font-semibold  focus:outline-none focus:shadow-outline hover:bg-blue-600 shadow-lg cursor-pointer transition ease-in duration-300">
                        Update Post
                    </button>
                </div>{{-- Submit section Ends --}}
            </form>
        </div>
    </div>

    {{-- <style>
        .has-mask {
            position: absolute;
            clip: rect(10px, 150px, 130px, 10px);
        }

    </style> --}}
</body>

<script src="{{ url('frontend/assets/js/admin.js') }}"></script>

</html>
