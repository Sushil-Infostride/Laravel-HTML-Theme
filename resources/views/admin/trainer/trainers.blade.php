@extends('admin.layout.main')


@section('main-container')
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

    <div class="w-full overflow-x-hidden border-t flex flex-col">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" />
        <table id="example" class="display" style="width:80%">
            <thead>
                <th>Trainer name</th>
                <th>field area</th>
                <th>content</th>
                <th>Profile url</th>
                <th>Actions</th>

            </thead>
            <tbody>
                @foreach ($trainers as $trainer)
                    <td align="center"><img style="border-radius:50%" width="50%"
                            src="{{ asset('trainers-upload-trainer') }}/{{ $trainer->trainer_image }}" class="img-fluid"
                            alt=""><b>{{ $trainer->trainer_name }}</b></td>
                    <td>{{ $trainer->field_area }}</td>
                    <td>{{ $trainer->content }}</td>
                    <td>
                        <i class="bi bi-facebook"> {{ $trainer->facebook }}<br>
                            <i class="bi bi-linkedin"> {{ $trainer->linkedin }}<br>
                                <i class="bi bi-twitter"> {{ $trainer->twitter }}<br>
                                    <i class="bi bi-instagram"> {{ $trainer->instagram }}<br>
                    </td>
                    <td> <a href="/admin/trainers/edit/{{ $trainer->id }}"><button type="button" id="any_button"
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                style="margin-bottom: 2px"><i class="fas fa-edit"></i></button></a>
                        <a href="/admin/trainers/delete/{{ $trainer->id }}"><button type="button" id="any_button"
                                class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"><i
                                    class="far fa-trash-alt"></i></button></a>
                    </td>

            </tbody>
            @endforeach
        </table>
    </div>
    @push('jquery')
        <script src="https://code.jquery.com/jquery-2.0.0.js" integrity="sha256-iW43nTNM8LFseNmWKhV5FHFW1KcjVQMvzg3l9nPU4oc="
                crossorigin="anonymous"></script>
    @endpush

    {{-- DATA table Push --}}
    @push('data-table-script')
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    @endpush

    <script>
        $(document).ready(function() {
            var table = $('#example').DataTable({
                dom: 'l<"toolbar">frtip',
                initComplete: function() {
                    $("div.toolbar")
                        .html(
                            '<a href="/admin/trainers/create"><button type="button"  id="any_button" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Add more </button></a>'
                        );
                },
                searching: false,
                paging: false,
                info: false
            });

        });
    </script>
@endsection
