@extends('admin.layout.main')
@section('main-container')
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
@section('title')
    <h1 class="title text-xl font-bold text-blue-700">About Page</h1>
@endsection
<div class="w-full overflow-x-hidden border-t flex flex-col">
    <a href="/admin/about/create"><button type="button" id="create_button"
            class="top-0 justify-center mt-0 py-2  px-4 float-right border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
            Create Page
        </button>
    </a>
    <div>
        {{-- Loop starts --}}
        @foreach ($about as $about)
            <div class="md:grid md:grid-cols-2 mt-0 md:gap-6">
                <div class="mt-0 md:mt-0 md:col-span-2">
                    {{-- Button Redirecting to Edit Page --}}

                    {{-- Form starts for displaying data --}}
                    <form action="/admin/about/create" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 bg-white space-y-6 sm:p-6">
                                <a href="/admin/about/edit/{{ $about->id }}"><button type="button"
                                        class=" top-0 justify-center mt-0 py-2  px-4 float-right border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                        Edit Page
                                    </button>
                                </a>

                                <div class="grid grid-cols-3 gap-6">

                                    <div class="col-span-3 sm:col-span-2">
                                        {{--  --}}
                                        <label for="Breadcrumb" class="block text-md font-medium text-gray-900">
                                            Breadcrumb
                                        </label>
                                        <div class="mt-1 flex rounded-md shadow-sm">
                                            <input readonly type="text" value="{{ $about->btitle }}" name="btitle"
                                                id="company-website"
                                                class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 p-2 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300"
                                                placeholder="Add a breadcrumb title">
                                        </div>
                                        <div class="mt-1 flex rounded-md shadow-sm">
                                            <textarea readonly id="about" name="bcontent" rows="3"
                                                class="shadow-sm focus:ring-indigo-500 p-3 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md"
                                                placeholder="you@example.com">{{ $about->bcontent }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                {{--  --}}
                                <div>
                                    <label for="about" class="block text-md font-medium text-gray-700">
                                        Main Section
                                    </label>
                                    <div class="mt-1">
                                        <textarea readonly id="about" name="main_title" rows="3"
                                            class="shadow-sm focus:ring-indigo-500 p-3 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md"
                                            placeholder="you@example.com">{{ $about->main_title }}</textarea>
                                    </div>

                                    {{-- bullet table starts --}}
                                    <div class="mt-1 pt-2">
                                        {{-- <form action="/admin/feature/create" method="POST">
                                            @csrf
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Key Points</th>
                                                        <th scope="col"><a href="javascritp:;"
                                                                class="btn btn-info addRow">+</a></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><input readonly type="text" value="{{$about->btitle}}" name="bullet[]" id="icon"
                                                                class="form-control"></td>
                                                        <td><input readonly type="hidden" name="id[]" id="id" class="form-control">
                                                        </td>
                                                        <td scope="col"><a href="javascritp:;"
                                                                class="btn btn-danger subRow">-</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="text-center"><input readonly type="submit" value="Save  Bullets"
                                                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ">
                                            </div>
                                        </form> --}}
                                        <label for="about" class="block text-sm font-medium text-gray-700">
                                            Bullets
                                        </label>
                                        <div class="mt-1 flex rounded-md shadow-sm">
                                            <input readonly type="text" value="{{ $about->bullet1 }}" name="bullet1"
                                                id="company-website"
                                                class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 p-3 block w-full rounded-none rounded-r-md sm:text-sm border-blue-300"
                                                placeholder="Bullet point 1">
                                        </div>
                                        <div class="mt-1 flex rounded-md shadow-sm">
                                            <input readonly type="text" value="{{ $about->bullet2 }}" name="bullet2"
                                                id="company-website"
                                                class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 p-3 block w-full rounded-none rounded-r-md sm:text-sm border-blue-300"
                                                placeholder="Bullet point 2">
                                        </div>
                                        <div class="mt-1 flex rounded-md shadow-sm">
                                            <input readonly type="text" value="{{ $about->bullet3 }}" name="bullet3"
                                                id="company-website"
                                                class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 p-3 block w-full rounded-none rounded-r-md sm:text-sm border-blue-300"
                                                placeholder="Bullet point 3">
                                        </div>
                                    </div>{{-- bullet table ends --}}

                                    {{-- BAse Content for main Section --}}
                                    <div class="mt-2">
                                        <label for="about" class="block text-sm font-medium text-gray-700">
                                            Base Content
                                        </label>
                                        <textarea readonly id="about" name="base_content" rows="3"
                                            class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 p-2 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md"
                                            placeholder="Add base content">{{ $about->base_content }}</textarea>
                                    </div>{{-- Base Content for main Section ends --}}

                                    {{-- Cover Photo section --}}
                                    <div style="width:100%;height:500px; padding:2px">
                                        <label class="block text-md font-medium text-gray-900 mt-2">
                                            Cover photo
                                        </label>
                                        <div class="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 w-2/3 h-4/5 border-gray-300 border-black rounded-md box"
                                            style="background-image: url('{{ asset('about-image') }}/{{ $about->cover_image }}'); ">

                                        </div>
                                    </div>{{-- cover section ends --}}

                                </div>

                            </div>
                    </form>
        @endforeach
    </div>
</div>
</div>


@if ($about->id == '')
{
<script>
    document.getElementById("create_button").style.display = "block";
</script>
}
@else{
<script>
    document.getElementById("create_button").style.display = "none";
</script>
}
@endif
@endsection
