@extends('admin.layout.main')
@section('main-container')
@section('title')
    <h1 class="title text-xl font-bold text-blue-700">Create About Page</h1>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
crossorigin="anonymous"></script>
<div class="w-full overflow-x-hidden border-t flex flex-col">
    <div>
        <div class="md:grid md:grid-cols-2 mt-0 md:gap-6">
            <div class="mt-0 md:mt-0 md:col-span-2">
                <form action="/admin/contact/edit" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="shadow sm:rounded-md sm:overflow-hidden">
                        <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                            <div class="grid grid-cols-3 gap-6">
                                <div class="col-span-3 sm:col-span-2">
                                    <label for="Breadcrumb" class="block text-md font-medium text-gray-900">
                                        Breadcrumb
                                    </label>
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                        <input type="text"  class="hidden" value="{{$contacts->id}}" name="id">
                                        <input type="text" value="{{$contacts->btitle}}" name="btitle" id="company-website"
                                            class="border-solid border-2 border-blue-100  flex-1 p-3 block w-full rounded-md  rounded-r-md sm:text-sm border-blue-300"
                                            placeholder="Add a breadcrumb title">
                                    </div>
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                        <textarea id="about" name="bcontent" rows="3"
                                            class="border-solid border-2 border-blue-100  flex-1 p-3 block w-full rounded-md  rounded-r-md sm:text-sm border-blue-300"
                                            placeholder="you@example.com">{{$contacts->bcontent}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <label for="about" class="block text-md font-medium text-gray-700">
                                    Iframe
                                </label>
                                <div class="mt-1">
                                    <textarea id="about" name="iframe" rows="3"
                                        class="border-solid border-2 border-blue-100  flex-1 p-3 block w-full rounded-md  rounded-r-md sm:text-sm border-blue-300"
                                        placeholder="you@example.com">{{$contacts->iframe}}</textarea>
                                </div>

                                <div class="mt-1 pt-2">

                                    <label for="about" class="block text-sm font-medium text-gray-700">
                                        Address
                                    </label>
                                    <div class="mt-1 flex">
                                        <div class="mt-1 w-1/4 rounded-md shadow-sm m-1">
                                            <input type="text" value="{{$contacts->address_icon}}" name="address_icon" id="company-website"
                                                class="border-solid border-2 border-blue-100  flex-1 p-3 block w-full rounded-md  rounded-r-md sm:text-sm border-blue-300"
                                                placeholder="Add adress icon">
                                        </div>
                                        <div class="mt-1 w-3/4 rounded-md shadow-sm">
                                            <input type="text" value="{{$contacts->address}}" name="address" id="company-website"
                                                class="border-solid border-2 border-blue-100  flex-1 p-3 block w-full rounded-md  rounded-r-md sm:text-sm border-blue-300"
                                                placeholder="Add address">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-1 pt-2">

                                    <label for="about" class="block text-sm font-medium text-gray-700">
                                        Email
                                    </label>
                                    <div class="mt-1 flex ">
                                        <div class="mt-1 w-1/4 rounded-md shadow-sm m-1">
                                            <input type="text" value="{{$contacts->email_icon}}" name="email_icon" id="company-website"
                                                class="border-solid border-2 border-blue-100  flex-1 p-3 block w-full rounded-md  rounded-r-md sm:text-sm border-blue-300"
                                                placeholder="Add adress icon">
                                        </div>
                                        <div class="mt-1 w-3/4 rounded-md shadow-sm">
                                            <input type="text" value="{{$contacts->email_add}}" name="email_add" id="company-website"
                                                class="border-solid border-2 border-blue-100  flex-1 p-3 block w-full rounded-md  rounded-r-md sm:text-sm border-blue-300"
                                                placeholder="Add email">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-1 pt-2">

                                    <label for="about" class="block text-sm font-medium text-gray-700">
                                        Location
                                    </label>
                                    <div class="mt-1 flex m-1">
                                        <div class="mt-1 w-1/4 rounded-md shadow-sm m-1">
                                            <input type="text" value="{{$contacts->location_icon}}" name="location_icon" id="company-website"
                                                class="border-solid border-2 border-blue-100 flex-1 p-3 block w-full rounded-md  rounded-r-md sm:text-sm border-blue-300"
                                                placeholder="Add adress icon">
                                        </div>
                                        <div class="mt-1 w-3/4 rounded-md shadow-sm">
                                            <input type="text" value="{{$contacts->location}}" name="location" id="company-website"
                                                class="border-solid border-2 border-blue-100  flex-1 p-3 block w-full rounded-md  rounded-r-md sm:text-sm border-blue-300"
                                                placeholder="Add location">
                                        </div>
                                    </div>
                                </div>
                                {{-- bullet table --}}

                            </div>
                            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                <button type="submit"
                                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-100">
                                    Update
                                </button>
                                <a href="/admin/contact"><button type="cancel"
                                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-100">
                                    Cancel
                                </button></a>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
    <script src="{{ url('frontend/assets/js/admin.js') }}"></script>
@endsection
