@extends('admin.layout.main')
@section('main-container')

    @push('DataTable-Css')
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" />
    @endpush

  <style>
      body{
          max-width:100%;
      }
  </style>

    {{-- Table to show a data --}}

    <table id="example" class="display" style="width:80%">
        <thead>
            <th>title</th>
            <th>field area</th>
            <th>content</th>
            <th>Price</th>
            <th>Trainer</th>
            <th>Featured Image</th>
            <th>Actions</th>

        </thead>
        <tbody>
            @foreach ($courses as $course)
                <form action="/admin/courses/edit" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <input type="hidden" name="id" value="{{ $course->id }}" />
                    <td><input type="text" name="title" class="myInput" readonly="readonly" value="{{ $course->title }}">
                    </td>
                    <td><input type="text" name="field_area" class="myInput" readonly="readonly"
                            value="{{ $course->field_area }}"></td>
                    <td><textarea name="content" class="myInput" cols="30" rows="5"
                            readonly="readonly">{{ $course->content }}</textarea></td>
                    <td><input type="text" name="" class="myInput" readonly value="{{ $course->price }}"></td>

                    {{-- Featured Image and Name --}}
                    <td align="center">
                        <img style="border-radius:50%" width="50%"
                            src="{{ asset('upload-trainer') }}/{{ $course->trainer_image }}" class="img-fluid" alt="">
                        <b>{{ $course->trainer_name }}</b>
                    </td>

                    {{-- Trainer Image and Name --}}
                    <td>
                        <img style="border-radius:10%;border:1px solid black;padding:5px" width="100%"
                            src="{{ asset('upload-featured') }}/{{ $course->featured_image }}" class="img-fluid"
                            alt="...">
                    </td>

                    <td>
                        {{-- normal operation --}}
                        <div class="normal-operation">
                            {{-- Edit button --}}
                            <a href="/admin/courses/edit/{{ $course->id }}">
                                <button type="button" id="any_butto"
                                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                    style="margin-bottom: 2px">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </a>

                            {{-- Delete Button --}}
                            <a href="/admin/courses/delete/{{ $course->id }}"><button type="button" id="any_button"
                                    class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </a>
                        </div>
                        {{-- edit operation --}}
                        <div class="edit-operation">
                            {{-- Save Button --}}
                            <button type="submit" id="any_button"
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 my-2 px-4 rounded">
                                <i class="fas fa-check"></i>
                            </button>
                            {{-- Cancel Button --}}
                            <a href="/admin/courses/"><button type="any" id="any_button"
                                    class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
                                    <i class="fas fa-window-close"></i>
                                </button>
                        </div>
                        </a>
                    </td>
                </form>
        </tbody>
        @endforeach
    </table>
    </div>
    {{-- Jquery Push --}}
    @push('jquery')
        <script src="https://code.jquery.com/jquery-2.0.0.js" integrity="sha256-iW43nTNM8LFseNmWKhV5FHFW1KcjVQMvzg3l9nPU4oc="
                crossorigin="anonymous"></script>
    @endpush

    {{-- DATA table Push --}}
    @push('data-table-script')
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    @endpush



    <script>
        $(document).ready(function() {
            $('.edit-operation').hide();
            $('.myInput').on("dblclick", function() {
                $(this).removeAttr("readonly");
                $('.edit-operation').show();
                $('.normal-operation').hide();

            });
            var table = $('#example').DataTable({
                dom: 'l<"toolbar">frtip',
                initComplete: function() {
                    $("div.toolbar")
                        .html(
                            '<a href="/admin/courses/create"><button type="button"  id="add-more" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Add more </button></a>'
                        );
                },
                searching: false,
                paging: false,
                info: false
            });

        });
    </script>
@endsection
