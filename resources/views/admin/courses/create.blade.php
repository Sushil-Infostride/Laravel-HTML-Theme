<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Post</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
</head>


<body>
    <div class="relative min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8 bg-gray-500 bg-no-repeat bg-cover relative items-center"
        style="background-image: url({{ asset('bg.jpg') }});">

        <div class="absolute bg-black opacity-60 inset-0 z-0"></div>
        <div class="sm:max-w-lg w-full p-10 bg-white rounded-xl z-10">
            <div class="text-center">
                <h2 class="mt-5 text-3xl font-bold text-gray-900">
                    Create Post
                </h2>
                <p class="mt-2 text-sm text-gray-400"> Lets Add a new Post by adding its whole information or<a
                        href="/admin/courses" class="bg-none text-blue-300 hover:text-blue-700 "> See All Posts</a> </p>
            </div>
            <form class="mt-8 space-y-3" action="{{ route('courses.store') }}" method="POST"
                enctype="multipart/form-data">
                @csrf
                @if (Session::has('course_added'))
                    <div class="flex items-center bg-blue-500 text-white text-sm font-bold px-4 py-3" role="alert">
                        <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path
                                d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z" />
                        </svg>
                        <p>{{ Session::get('course_added') }} <a href="/admin/courses"
                                class="bg-none text-blue-300 hover:text-blue-700 "> See All Posts</a></p>
                    </div>
                @endif
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Title of Post</label>
                    <input name="title"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500"
                        type="" placeholder="Add a beautiful title to your post">
                </div>
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Area Of Field</label>
                    <input name="field_area"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500"
                        type="" placeholder="Eg. Web-design, Android, Marketing ....">
                </div>
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Price</label>
                    <input name="price"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500"
                        type="" placeholder="Add the price">
                </div>
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Content</label>
                    <textarea name="content"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500"
                        type="" placeholder="Add Your Content"></textarea>
                </div>
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Trainer name</label>
                    <input name="trainer_name"
                        class="text-base p-2 border border-gray-300 rounded-lg focus:outline-none focus:border-indigo-500"
                        type="" placeholder="Add Trainer name">
                </div>
                {{-- featured Image --}}
                <div class="grid grid-cols-1 space-y-2">
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Attach Featured Image</label>
                    <div class="flex items-center justify-center w-full box-1">
                        <label
                            class="flex flex-col rounded-lg border-4 border-dashed w-full h-60 p-10 group text-center">
                            <div
                                class="h-full w-full text-center flex flex-col items-center justify-center items-center featured-icon ">
                                <div class="flex flex-auto max-h-48 w-2/5 mx-auto -mt-10">
                                    <img class="has-mask h-36 object-center"
                                        src="{{ url('frontend/assets/img/upload-icon.jpg') }}" alt="freepik image">
                                </div>
                                <p class="pointer-none text-gray-500 "><span class="text-sm">Add Your Post featured
                                        image</span> files
                                    here <br /> or<input type="file" id="featured-image" name="featured-image" class="hidden"
                                onchange="previewfile1(this)">
                            </div>

                        </label>
                    </div>
                </div>
                {{-- featured Image Ends --}}

                <div class="grid grid-cols-1 space-y-2 ">
                    {{-- Trainer Image starts --}}
                    <label class="text-sm font-bold text-gray-500 tracking-wide">Attach Trainer Image</label>
                    <div class="flex items-center justify-center w-full box">
                        <label
                            class="flex flex-col rounded-lg border-4 border-dashed w-full h-60 p-10 group text-center">
                            <div
                                class="h-full w-full text-center flex flex-col items-center justify-center items-center trainer-icon  ">
                                <div class="flex flex-auto max-h-48 w-2/5 mx-auto -mt-10 ">
                                    <img class="has-mask h-36 object-center"
                                        src="{{ url('frontend/assets/img/upload-icon.jpg') }}" alt="freepik image">
                                </div>
                                <p class="pointer-none text-gray-500 "><span class="text-sm">Add Trainer image</span>
                                    files
                                    here <br /><input type="file" id="post-image" class="hidden"
                                    name="trainer-image" onchange="previewfile(this)">
                            </div>

                        </label>
                    </div>
                </div>
                {{-- Trainer Image ends --}}
                <div>
                    <button type="submit"
                        class="my-5 w-full flex justify-center bg-blue-500 text-gray-100 p-4  rounded-full tracking-wide
                                    font-semibold  focus:outline-none focus:shadow-outline hover:bg-blue-600 shadow-lg cursor-pointer transition ease-in duration-300">
                        Create Post
                    </button>
                </div>
            </form>
        </div>
    </div>

    <style>
        .has-mask {
            position: absolute;
            clip: rect(10px, 150px, 130px, 10px);
        }

    </style>
</body>

<script src="{{ url('frontend/assets/js/admin.js') }}"></script>

</html>
