@extends('admin.layout.main')
@section('main-container')
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
crossorigin="anonymous"></script>
    <!-- ======= Hero Section ======= -->
    <div class="w-full overflow-x-hidden border-t flex flex-col">
        <div>
            <div class="md:grid md:grid-cols-3 md:gap-6">

                <div class="mt-0 md:mt-0 md:col-span-8">
                    <form action="/admin/testimonial/update" method="POST" enctype="multipart/form-data">
                        @csrf

                        {{-- Alert message --}}
                        @if (Session::has('testimonial_updated'))
                            <div class="flex items-center bg-blue-500 text-white text-sm font-bold px-4 py-3" role="alert">
                                <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 20 20">
                                    <path
                                        d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z" />
                                </svg>
                                <p>{{ Session::get('testimonial_updated') }} <a href="/admin/courses"
                                        class="bg-none text-blue-300 hover:text-blue-700 "> See All Posts</a> </p>
                            </div>
                        @endif

                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                <div class="grid grid-cols-3 gap-6">
                                    <div class="col-span-3 sm:col-span-2">
                                        <input type="hidden" name="id" id="" value="{{ $testimonials->id }}">
                                        <label for="label" class="block text-sm font-medium text-gray-700">
                                            Name
                                        </label>
                                        <div class="mt-1 flex rounded-md shadow-sm">
                                            <input type="text" name="name" id="label" value="{{ $testimonials->name }}"
                                                class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300"
                                                placeholder=" http://127.0.0.1:8000/about">
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <label for="about" class="block text-sm font-medium text-gray-700">
                                        Designation
                                    </label>
                                    <div class="mt-1">
                                        <input type="text" name="post" id="label" value="{{$testimonials->post}}"
                                            class="focus:ring-indigo-500 focus:border-indigo-500 p-2 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300"
                                            placeholder=" http://127.0.0.1:8000/about">
                                    </div>

                                </div>
                                <div>
                                    <label for="about" class="block text-sm font-medium text-gray-700">
                                        What the say ...
                                    </label>
                                    <div class="mt-1">
                                        <textarea id="about" name="content" rows="3"
                                            class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1  p-2  block w-full sm:text-sm border-gray-300 rounded-md"
                                            placeholder="What are the saying..."> {{$testimonials->content}}"</textarea>
                                    </div>
                                    <p class="mt-2 text-sm text-gray-500">
                                        Write what they have a say on us
                                    </p>
                                </div>
                                <div>
                                    <label class="block text-sm font-medium text-gray-700 ">
                                        Profile Image
                                    </label>
                                    <div
                                        class="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md box"style="background-image: url('{{ asset('Testimonial-image') }}/{{ $testimonials->avatar }}')">
                                        <div class="space-y-1 text-center ">
                                            <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none"
                                                viewBox="0 0 48 48" aria-hidden="true">
                                                <path
                                                    d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                                    stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            <div class="flex text-sm text-gray-600">
                                                <label for="cover_image"
                                                    class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                    <span>Upload a file</span>
                                                    <input type="text" class="hidden" name="avatar_old_image" value="{{$testimonials->avatar}}" />
                                                    <input type="file" class="hidden" id="cover_image" name="avatar"
                                                        onchange="previewfileheader(this)">
                                                </label>
                                                <p class="pl-1">or drag and drop</p>
                                            </div>
                                            <p class="text-xs text-gray-500">
                                                PNG, JPG, GIF up to 10MB
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                <button type="submit"
                                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script src="{{ url('frontend/assets/js/admin.js') }}"></script>
        <!-- ======= Footer ======= -->
    @endsection
