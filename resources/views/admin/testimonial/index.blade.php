@extends('admin.layout.main')
@section('main-container')

    @push('DataTable-Css')
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" />
    @endpush

@section('title')
    <h1 class="title text-xl font-bold text-blue-700">All Headers</h1>
@endsection

@if (Session::has('testimonial_deleted'))
<div class="flex items-center bg-blue-500 text-white text-sm font-bold px-4 py-3" role="alert">
    <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20">
        <path
            d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z" />
    </svg>
    <p>{{ Session::get('testimonial_deleted') }} <a href="/admin/courses"
            class="bg-none text-blue-300 hover:text-blue-700 "> See All Posts</a> </p>
</div>
@endif

{{-- Table to show a data --}}
<div class="w-full overflow-x-hidden border-t flex flex-col">
<table id="example" class="display" style="width:80%">
    <thead>
        <th>Client</th>
        <th>Designation</th>
        <th>What they say</th>
        <th>Actions</th>
    </thead>
    <tbody>
        <form action="/admin/testimonial" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @foreach ($testimonial as $testimonial)


            <input type="hidden" name="id" value="{{ $testimonial->id }}" />
            <td class="text-center"><img style="border-radius:50%" width="60%"
                    src="{{ asset('Testimonial-image') }}/{{ $testimonial->avatar }}" class="img-fluid"
                    alt=""><input type="text" name="name" id="label" value="{{ $testimonial->name }}"
                    class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300"
                    placeholder=" http://127.0.0.1:8000/about">
            </td>
            <td><input type="text" value="{{ $testimonial->post }}" name="post" id="company_website"
                    class="focus:ring-indigo-500 focus:border-indigo-500 p-2  flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300"
                    placeholder=" http://127.0.0.1:8000/about">
            </td>
            <td>
                <textarea id="about" name="content" rows="3"
                    class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1  p-2  block w-full sm:text-sm border-gray-300 rounded-md"
                    placeholder="you@example.com">{{ $testimonial->content }}</textarea>
            </td>
            <td>
                {{-- normal operation --}}
                <div class="normal-operation">
                    {{-- Edit button --}}
                    <a href="/admin/testimonial/edit/{{ $testimonial->id }}">
                        <button type="button" id="any_butto"
                            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                            style="margin-bottom: 2px">
                            <i class="fas fa-edit"></i>
                        </button>
                    </a>

                    {{-- Delete Button --}}
                    <a href="/admin/testimonial/delete/{{ $testimonial->id }}"><button type="button" id="any_button"
                            class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
                            <i class="far fa-trash-alt"></i>
                        </button>
                    </a>
                </div>
            </td>
        </form>
    </tbody>
    @endforeach
</table>
</div>
{{-- Jquery Push --}}
@push('jquery')
    <script src="https://code.jquery.com/jquery-2.0.0.js" integrity="sha256-iW43nTNM8LFseNmWKhV5FHFW1KcjVQMvzg3l9nPU4oc="
        crossorigin="anonymous"></script>
@endpush

{{-- DATA table Push --}}
@push('data-table-script')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
@endpush



<script>
    $(document).ready(function() {
        $('.edit-operation').hide();
        $('.myInput').on("dblclick", function() {
            $(this).removeAttr("readonly");
            $('.edit-operation').show();
            $('.normal-operation').hide();

        });
        var table = $('#example').DataTable({
            dom: 'l<"toolbar">frtip',
            initComplete: function() {
                $("div.toolbar")
                    .html(
                        '<a href="/admin/testimonial/create"><button type="button"  id="add-more" class="bg-blue-500 hover:bg-blue-700 text-white float-right mt-2 mr-4 font-bold py-2 px-4 rounded">Add more </button></a>'
                    );
            },
            searching: false,
            paging: false,
            info: false
        });

    });
</script>
@endsection
