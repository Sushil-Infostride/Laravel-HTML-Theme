<!DOCTYPE html>
<html lang="en">

<head>
    <title>Admin Dashboard</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Bootstrap Main CSS File -->
    <link href="{{ url('frontend/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link rel="stylesheet" href="{{ url('frontend/assets/css/amdinStyle.css') }}" />

    <!-- Tailwind -->
    <link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

    <!-- Data tables -->
    @stack('DataTable-Css')

</head>

<body class="bg-gray-100 font-family-karla flex">

    <aside class="relative bg-sidebar h-screen w-64 hidden sm:block shadow-xl overflow-auto">
        <div class="p-6">
            <a href="/admin/header" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">MENTOR</a>
        </div>
        <nav class="text-white text-base font-semibold pt-3">
            <a href="/admin/header"
                class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item {{ request()->is('admin/header') ? 'active-nav-link' : '' }}">
                <i class="fas fa-tachometer-alt mr-3"></i>
                Header
            </a>
            <a href="/admin/testimonial"
                class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item {{ request()->is('admin/testimonial') ? 'active-nav-link' : '' }}">
                <i class="fas fa-tachometer-alt mr-3"></i>
                Testimonial
            </a>
            <a href="/admin/feature"
                class="flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item {{ request()->is('admin/feature') ? 'active-nav-link' : '' }}">
                <i class="fas fa-tachometer-alt mr-3"></i>
                Feature
            </a>
            <a href="/admin/about"
                class=" {{ request()->is('admin/about') ? 'active-nav-link' : '' }} flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
                <i class="fas fa-sticky-note mr-3"></i>
                About
            </a>
            <a href="/admin/courses"
                class="{{ request()->is('admin/courses') ? 'active-nav-link' : '' }} flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
                <i class="fas fa-table mr-3"></i>
                Courses
            </a>
            <a href="/admin/trainers"
                class="{{ request()->is('admin/trainers') ? 'active-nav-link' : '' }} flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
                <i class="fas fa-align-left mr-3"></i>
                Trainers
            </a>
            <a href="/admin/events"
                class=" {{ request()->is('admin/events') ? 'active-nav-link' : '' }} flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
                <i class="fas fa-tablet-alt mr-3"></i>
                Events
            </a>

            <a href="/admin/contact"
                class=" {{ request()->is('admin/contact') ? 'active-nav-link' : '' }} flex items-center text-white opacity-75 hover:opacity-100 py-4 pl-6 nav-item">
                <i class="fas fa-calendar mr-3"></i>
                Contact
            </a>
        </nav>
    </aside>

    <div class="w-full flex flex-col h-screen overflow-y-hidden">
        <!-- Desktop Header -->
        <header class="w-full items-center bg-white py-2 px-6 hidden sm:flex">
            <div class="w-1/2">
                @yield('title')
            </div>
            <div x-data="{ isOpen: false }" class="relative w-1/2 flex justify-end">

                <a href="/logout" class="block px-4 py-2 account-link hover:text-white z-10">Sign Out</a>

            </div>
        </header>

        <!-- Mobile Header & Nav -->
        <header x-data="{ isOpen: false }" class="w-full bg-sidebar py-5 px-6 sm:hidden">
            <div class="flex items-center justify-between">
                <a href="index.html" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">Admin</a>
                <button @click="isOpen = !isOpen" class="text-white text-3xl focus:outline-none">
                    <i x-show="!isOpen" class="fas fa-bars"></i>
                    <i x-show="isOpen" class="fas fa-times"></i>
                </button>
            </div>

            <!-- Dropdown Nav -->
            <nav :class="isOpen ? 'flex': 'hidden'" class="flex flex-col pt-4">
                <a href="index.html" class="flex items-center active-nav-link text-white py-2 pl-4 nav-item">
                    <i class="fas fa-tachometer-alt mr-3"></i>
                    Home
                </a>
                <a href="blank.html"
                    class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
                    <i class="fas fa-sticky-note mr-3"></i>
                    About
                </a>
                <a href="tables.html"
                    class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
                    <i class="fas fa-table mr-3"></i>
                    Courses
                </a>
                <a href="forms.html"
                    class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
                    <i class="fas fa-align-left mr-3"></i>
                    Contact
                </a>
                <a href="tabs.html"
                    class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
                    <i class="fas fa-tablet-alt mr-3"></i>
                    Pricings
                </a>
                <a href="calendar.html"
                    class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
                    <i class="fas fa-calendar mr-3"></i>
                    Events
                </a>
                {{-- <a href="#" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
                    <i class="fas fa-cogs mr-3"></i>
                    Nothing
                </a>
                <a href="#" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
                    <i class="fas fa-user mr-3"></i>
                    My Account
                </a> --}}
                <a href="/logout" class="flex items-center text-white opacity-75 hover:opacity-100 py-2 pl-4 nav-item">
                    <i class="fas fa-sign-out-alt mr-3"></i>
                    Sign Out
                </a>

            </nav>

        </header>


        @stack('jquery')
        @stack('data-table-script')
