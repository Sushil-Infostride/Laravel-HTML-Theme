@extends('admin.layout.main')
@section('main-container')

    @push('DataTable-Css')
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" />
    @endpush

    @section('title')
        <h1 class="title text-xl font-bold text-blue-700">All Headers</h1>
    @endsection


{{-- Table to show a data --}}

<table id="example" class="display" style="width:80%">
    <thead>
        <th>Titles</th>
        <th>Subtitle</th>
        <th>content</th>
        <th>Button_name</th>
        <th>Button link</th>
        <th>Cover Image</th>
        <th>Actions</th>
    </thead>
    <tbody>
        @foreach ($headers as $header)
            <form action="/admin/header/edit" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <input type="hidden" name="id" value="{{ $header->id }}" />
                <td><input type="text" value="{{ $header->title }}" name="title" id="company_website"
                        class="focus:ring-indigo-500 focus:border-indigo-500 p-2  flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300"
                        placeholder=" http://127.0.0.1:8000/about">
                </td>
                <td><input type="text" value="{{ $header->subtitle }}" name="subtitle" id="company_website"
                        class="focus:ring-indigo-500 focus:border-indigo-500 p-2  flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300"
                        placeholder=" http://127.0.0.1:8000/about">
                </td>
                <td><textarea id="about" name="content" rows="3"
                        class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1  p-2  block w-full sm:text-sm border-gray-300 rounded-md"
                        placeholder="you@example.com">{{ $header->content }}</textarea></td>
                <td> <input type="text" value="{{ $header->button_name }}" name="button_name" id="company_website"
                        class="focus:ring-indigo-500 focus:border-indigo-500 p-2 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300"
                        placeholder=" http://127.0.0.1:8000/about">
                </td>
                <td><input type="text" value="{{ $header->button_link }}" name="button_link" id="company_website"
                        class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300"
                        placeholder=" http://127.0.0.1:8000/about"></td>

                {{-- Featured Image and Name --}}
                <td align="center">
                    <img style="border-radius:1%" width="100%"
                        src="{{ asset('header-image') }}/{{ $header->cover_image }}" class="img-fluid" alt="">
                    <b>{{ $header->trainer_name }}</b>
                </td>

                {{-- Trainer Image and Name --}}
                <td>
                    {{-- normal operation --}}
                    <div class="normal-operation">
                        {{-- Edit button --}}
                        <a href="/admin/header/edit/{{ $header->id }}">
                            <button type="button" id="any_butto"
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                style="margin-bottom: 2px">
                                <i class="fas fa-edit"></i>
                            </button>
                        </a>

                        {{-- Delete Button --}}
                        <a href="/admin/header/delete/{{ $header->id }}"><button type="button" id="any_button"
                                class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </a>
                    </div>
                </td>
            </form>
    </tbody>
    @endforeach
</table>
</div>
{{-- Jquery Push --}}
@push('jquery')
    <script src="https://code.jquery.com/jquery-2.0.0.js" integrity="sha256-iW43nTNM8LFseNmWKhV5FHFW1KcjVQMvzg3l9nPU4oc="
        crossorigin="anonymous"></script>
@endpush

{{-- DATA table Push --}}
@push('data-table-script')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
@endpush



<script>
    $(document).ready(function() {
        $('.edit-operation').hide();
        $('.myInput').on("dblclick", function() {
            $(this).removeAttr("readonly");
            $('.edit-operation').show();
            $('.normal-operation').hide();

        });
        var table = $('#example').DataTable({
            dom: 'l<"toolbar">frtip',
            initComplete: function() {
                $("div.toolbar")
                    .html(
                        '<a href="/admin/header/create"><button type="button"  id="add-more" class="bg-blue-500 hover:bg-blue-700 text-white float-right mt-2 mr-4 font-bold py-2 px-4 rounded">Add more </button></a>'
                    );
            },
            searching: false,
            paging: false,
            info: false
        });

    });
</script>
@endsection
