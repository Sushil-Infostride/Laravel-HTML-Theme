@extends('frontend.layout.main')


@section('main-container')

    <main id="main">
        @foreach ($about as $about)


            <!-- ======= Breadcrumbs ======= -->
            <div class="breadcrumbs" data-aos="fade-in">
                <div class="container">
                    <h2>{{ $about->btitle }}</h2>
                    <p>{{ $about->bcontent }} </p>
                </div>
            </div><!-- End Breadcrumbs -->

            <!-- ======= About Section ======= -->
            <section id="about" class="about">
                <div class="container" data-aos="fade-up">

                    <div class="row">
                        <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
                            <img src="{{ asset('about-image') }}/{{ $about->cover_image }}" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
                            <h3>{{ $about->main_title }}</h3>
                            <p class="fst-italic">
                                {{ $about->base_content }}
                            </p>
                            <ul>
                                <li><i class="bi bi-check-circle"></i> {{ $about->bullet1 }}</li>
                                <li><i class="bi bi-check-circle"></i> {{ $about->bullet2 }}</li>
                                <li><i class="bi bi-check-circle"></i> {{ $about->bullet3 }}</li>
                            </ul>
                            <p>
                                {{ $about->base_content }}
                            </p>

                        </div>
                    </div>
        @endforeach
        </div>
        </section>

        <!-- ======= Testimonials Section ======= -->
        <section id="testimonials" class="testimonials">
            <div class="container" data-aos="fade-up">

                <div class="section-title">
                    <h2>Testimonials</h2>
                    <p>What are they saying</p>
                </div>

                <div class="testimonials-slider swiper-container" data-aos="fade-up" data-aos-delay="100">
                    <div class="swiper-wrapper">
                        @foreach ($testimonials as $testimonials)
                            <div class="swiper-slide">
                                <div class="testimonial-wrap">
                                    <div class="testimonial-item">
                                        <img src="{{ asset('Testimonial-image') }}/{{ $testimonials->avatar }}"
                                            class="testimonial-img" alt="">
                                        <h3>{{ $testimonials->name }}</h3>
                                        <h4>{{ $testimonials->post }}</h4>
                                        <p>
                                            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                            {{ $testimonials->content }}
                                            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                        </p>
                                    </div>
                                </div>
                            </div><!-- End testimonial item -->
                        @endforeach
                        <!-- End testimonial item -->

                    </div>
                    <div class="swiper-pagination"></div>
                </div>

            </div>
        </section><!-- End Testimonials Section -->

    </main><!-- End #main -->
@endsection
