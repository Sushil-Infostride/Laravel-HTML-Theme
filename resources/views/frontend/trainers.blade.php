@extends('frontend.layout.main')


@section('main-container')

  <main id="main" data-aos="fade-in">

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs">
      <div class="container">
        <h2>Trainers</h2>
        <p>Est dolorum ut non facere possimus quibusdam eligendi voluptatem. Quia id aut similique quia voluptas sit quaerat debitis. Rerum omnis ipsam aperiam consequatur laboriosam nemo harum praesentium. </p>
      </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= Trainers Section ======= -->
    <!-- ======= Trainers Section ======= -->
    <section id="trainers" class="trainers">
        <div class="container" data-aos="fade-up">

          <div class="row" data-aos="zoom-in" data-aos-delay="100">
              @foreach ($trainers as $trainer )


            <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
              <div class="member">
                  <img src="{{ asset('trainers-upload-trainer') }}/{{ $trainer->trainer_image }}" class="img-fluid" alt="">
                <div class="member-content">
                  <h4>{{$trainer->trainer_name}}</h4>
                  <span>{{$trainer->field_area}}</span>
                  <p>
                      {{$trainer->content}}
                  </p>
                  <div class="social">
                    <a href="{{$trainer->twitter}}"><i class="bi bi-twitter"></i></a>
                    <a href="{{$trainer->facebook}}"><i class="bi bi-facebook"></i></a>
                    <a href="{{$trainer->instagram}}"><i class="bi bi-instagram"></i></a>
                    <a href="{{$trainer->linkedin}}"><i class="bi bi-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>

        </div>
      </section><!-- End Trainers Section -->

  </main><!-- End #main -->
</div>
  @endsection
