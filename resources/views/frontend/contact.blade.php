@extends('frontend.layout.main')


@section('main-container')
    <main id="main">
        @foreach ($contacts as $contacts)

            <!-- ======= Breadcrumbs ======= -->
            <div class="breadcrumbs" data-aos="fade-in">
                <div class="container">
                    <h2>{{ $contacts->btitle }}</h2>
                    <p>{{ $contacts->bcontent }} </p>
                </div>
            </div><!-- End Breadcrumbs -->

            <!-- ======= Contact Section ======= -->
            <section id="contact" class="contact">
                <div data-aos="fade-up">
                    <iframe style="border:0; width: 100%; height: 350px;" src="{{ $contacts->iframe }}" frameborder="0"
                        allowfullscreen></iframe>
                </div>

                <div class="container" data-aos="fade-up">

                    <div class="row mt-5">

                        <div class="col-lg-4">
                            <div class="info">
                                <div class="address">
                                    <i class="{{ $contacts->address_icon }}"></i>
                                    <h4>Location:</h4>
                                    <p>{{ $contacts->address }}</p>
                                </div>

                                <div class="email">
                                    <i class="{{ $contacts->location_icon}}"></i>
                                    <h4>Email:</h4>
                                    <p>{{ $contacts->email_add }}</p>
                                </div>

                                <div class="phone">
                                    <i class="{{ $contacts->location_icon }}"></i>
                                    <h4>Call:</h4>
                                    <p>{{ $contacts->location }}</p>
                                </div>

                            </div>

                        </div>

                        <div class="col-lg-8 mt-5 mt-lg-0">

                            <form action="{{ route('contact.send') }}" method="POST" role="form">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="name" class="form-control" id="name"
                                            placeholder="Your Name" required>
                                    </div>
                                    <div class="col-md-6 form-group mt-3 mt-md-0">
                                        <input type="email" class="form-control" name="email" id="email"
                                            placeholder="Your Email" required>
                                    </div>
                                </div>
                                <div class="form-group mt-3">
                                    <input type="text" class="form-control" name="subject" id="subject"
                                        placeholder="Subject" required>
                                </div>
                                <div class="form-group mt-3">
                                    <textarea class="form-control" name="message" rows="5" placeholder="Message"
                                        required></textarea>
                                </div>
                                <div class="my-3">
                                    <div class="error-message"></div>
                                    <div class="sent-message">
                                        @if (Session::has('message-sent'))
                                            <div class="alert alert-primary" role="alert">
                                                {{ Session::get('message-sent') }}
                                            </div>

                                        @endif
                                    </div>
                                    <div class="text-center"><button type="submit">Send Message</button></div>
                            </form>

                        </div>

                    </div>

                </div>
            </section><!-- End Contact Section -->

        @endforeach
    </main>
@endsection
