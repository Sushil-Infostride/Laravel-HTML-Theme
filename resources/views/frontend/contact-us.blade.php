<style>
    h1, h2, h3, h4, h5, h6{
        color: #000;
    font-family: "Montserrat", Arial, sans-serif;
    font-weight: 700;
    margin: 0 0 30px 0;
    }
    #fh5co-main {

    -webkit-transition: 0.5s;
    -o-transition: 0.5s;
    transition: 0.5s;
    }
    #fh5co-main .fh5co-narrow-content {
    position: relative;
    width: 80%;
    margin: 0 auto;
    padding: 4em 0;
    }
    .form-control{
        -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    box-shadow: none;
    background: transparent;
    border: 2px solid rgba(0, 0, 0, 0.1) !important;
    height: 54px !important;
    font-size: 18px;
    font-weight: 300;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    -ms-border-radius: 0px;
    border-radius: 0px !important;
    }
    #message {
    height: 130px !important;
    }
    .btn.btn-md {
    padding: 10px 25px !important;
    }
    .btn-primary {
    background: #da1212 !important;
    color: #fff !important;
    border: 2px solid #da1212 !important;
    border-radius: 0px !important;
    }
</style>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<div id="fh5co-main">
<div class="fh5co-narrow-content animate-box fadeInLeft animated" data-animate-effect="fadeInLeft">

				<div class="row">
					<div class="col-md-4 col-md-offset-1">
						<h1>Get In Touch</h1>
					</div>

				</div>
        <br /><br />


					<div class="row">


						<div class="col-lg-8 mt-5 mt-lg-0">

                            <form action="/tempsend" method="post" role="form" class="php-email-form">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Your Name"
                                            required>
                                    </div>
                                    <div class="col-md-6 form-group mt-3 mt-md-0">
                                        <input type="email" class="form-control" name="email" id="email"
                                            placeholder="Your Email" required>
                                    </div>
                                </div>
                                <div class="form-group mt-3">
                                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject"
                                        required>
                                </div>
                                <div class="form-group mt-3">
                                    <textarea class="form-control" name="message" rows="5" placeholder="Message"
                                        required></textarea>
                                </div>
                                <div class="my-3">
                                    <div class="loading">Loading</div>
                                    <div class="error-message"></div>
                                    <div class="sent-message">
                                    @if (Session::has('message-sent'))
                                        {{Session::get('message-sent')}}
                                    @endif
                                    </div>
                                <div class="text-center"><button type="submit">Send Message</button></div>
                            </form>

                        </div>

                    </div>
				</form>
			</div>
</div>
