@extends('frontend.layout.main')
@section('main-container')
    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex justify-content-center align-items-center"
        style="background-image:url('{{ url('frontend/assets/img/hero-bg.jpg') }}')">
        @foreach ($header as $header)
            <div class="container position-relative" data-aos="zoom-in" data-aos-delay="100">
                <h1>{{ $header->title }},<br>{{ $header->subtitle }}</h1>
                <h2>{{ $header->content }}</h2>
                <a href="{{ $header->button_link }}" class="btn-get-started">{{ $header->button_name }}</a>
            </div>
    </section><!-- End Hero -->
    @endforeach
    <main id="main">

        <!-- ======= About Section ======= -->
        <section id="about" class="about">
            <div class="container" data-aos="fade-up">
                @foreach ($about as $about)
                    <div class="row">
                        <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
                            <img src="{{ url('frontend/assets/img/about.jpg') }}" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
                            <h3>{{ $about->main_title }}</h3>
                            <p class="fst-italic">
                                {{ $about->base_content }}
                            </p>
                            <ul>
                                <li><i class="bi bi-check-circle"></i> {{ $about->bullet1 }}</li>
                                <li><i class="bi bi-check-circle"></i> {{ $about->bullet2 }}</li>
                                <li><i class="bi bi-check-circle"></i> {{ $about->bullet3 }}</li>
                            </ul>
                            <p>
                                {{ $about->base_content }}
                            </p>

                        </div>
                    </div>
                @endforeach
            </div>
        </section>
        <!-- ======= Features Section ======= -->
        <section id="features" class="features">
            <div class="container" data-aos="fade-up">

                <div class="row" data-aos="zoom-in" data-aos-delay="100">
                    @foreach ($features as $features)
                        <div class="col-lg-3 col-md-4">
                            <div class="icon-box">
                                <i class="{{ $features->icon }}" style="color: #ffbb2c;"></i>
                                <h3><a href="">{{ $features->title }}</a></h3>
                            </div>
                        </div>
                    @endforeach
                </div>
        </section><!-- End Features Section -->

        <!-- ======= Popular Courses Section ======= -->
        <section id="popular-courses" class="courses">
            <div class="container" data-aos="fade-up">

                <div class="section-title">
                    <h2>Courses</h2>
                    <p>Popular Courses</p>
                </div>

                <div class="row" data-aos="zoom-in" data-aos-delay="100">
                    @foreach ($course as $course)
                        <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                            <div class="course-item">
                                <img src="{{ asset('upload-featured') }}/{{ $course->featured_image }}"
                                    class="img-fluid" alt="...">
                                <div class="course-content">
                                    <div class="d-flex justify-content-between align-items-center mb-3">
                                        <h4>{{ $course->title }}</h4>
                                        <p class="price">{{ $course->price }}</p>
                                    </div>

                                    <h3><a href="/coursedetails">{{ $course->field_area }}</a></h3>
                                    <p>{{ $course->content }}</p>
                                    <div class="trainer d-flex justify-content-between align-items-center">
                                        <div class="trainer-profile d-flex align-items-center">
                                            <img src="{{ asset('upload-trainer') }}/{{ $course->trainer_image }}"
                                                class="img-fluid" alt="">
                                            <span>{{ $course->trainer_name }}</span>
                                        </div>
                                        <div class="trainer-rank d-flex align-items-center">
                                            <i class="bx bx-user"></i>&nbsp;50
                                            &nbsp;&nbsp;
                                            <i class="bx bx-heart"></i>&nbsp;65
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <!-- End Course Item-->
                </div>

            </div>
        </section><!-- End Popular Courses Section -->

        <!-- ======= Trainers Section ======= -->
        <section id="trainers" class="trainers">
            <div class="container" data-aos="fade-up">

                <div class="row" data-aos="zoom-in" data-aos-delay="100">
                    @foreach ($trainer as $trainer)
                        <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                            <div class="member">
                                <img src="{{ asset('trainers-upload-trainer') }}/{{ $trainer->trainer_image }}"
                                    class="img-fluid" alt="">
                                <div class="member-content">
                                    <h4>{{ $trainer->trainer_name }}</h4>
                                    <span>{{ $trainer->field_area }}</span>
                                    <p>
                                        {{ $trainer->content }}
                                    </p>
                                    <div class="social">
                                        <a href="{{ $trainer->twitter }}"><i class="bi bi-twitter"></i></a>
                                        <a href="{{ $trainer->facebook }}"><i class="bi bi-facebook"></i></a>
                                        <a href="{{ $trainer->instagram }}"><i class="bi bi-instagram"></i></a>
                                        <a href="{{ $trainer->linkedin }}"><i class="bi bi-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{-- end --}}
                </div>

            </div>
        </section><!-- End Trainers Section -->

    </main><!-- End #main -->
@endsection
<!-- ======= Footer ======= -->
